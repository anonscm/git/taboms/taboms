/*
 * taBoMS,
 * tarent Book Management System user interface
 * Copyright (C) 2000-2009 tarent GmbH
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License,version 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 *
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'taBoMS'
 * Signature of Elmar Geese, 21 April 2009
 * Elmar Geese, CEO tarent GmbH.
 */

package de.tarent.taBoMS.client.i18n;

public class TaBoMSI18N_ implements de.tarent.taBoMS.client.i18n.TaBoMSI18N {
  public java.lang.String publisherHomepage() {
    return "Homepage";
  }
  
  public java.lang.String publisher() {
    return "Publisher-Information";
  }
  
  public java.lang.String authorName() {
    return "Name";
  }
  
  public java.lang.String subject() {
    return "Subject";
  }
  
  public java.lang.String location() {
    return "Location";
  }
  
  public java.lang.String isbn() {
    return "ISBN";
  }
  
  public java.lang.String saveButton() {
    return "save";
  }
  
  public java.lang.String general() {
    return "General-Information";
  }
  
  public java.lang.String author() {
    return "Author-Information";
  }
  
  public java.lang.String launch() {
    return "Launch";
  }
  
  public java.lang.String title() {
    return "Title";
  }
  
  public java.lang.String langPicDE() {
    return "German";
  }
  
  public java.lang.String description() {
    return "Description";
  }
  
  public java.lang.String langPicEN() {
    return "English";
  }
  
  public java.lang.String publisherName() {
    return "Name";
  }
  
  public java.lang.String windowHeader() {
    return "Book";
  }
  
  public java.lang.String owner() {
    return "Owner";
  }
  
  public java.lang.String newBook() {
    return "add a new Book";
  }
  
  public java.lang.String windowBookInfo() {
    return "Book-Information";
  }
  
  public java.lang.String preview() {
    return "preview";
  }
  
  }
