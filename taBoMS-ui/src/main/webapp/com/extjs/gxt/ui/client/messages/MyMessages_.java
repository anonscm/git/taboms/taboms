/*
 * taBoMS,
 * tarent Book Management System user interface
 * Copyright (C) 2000-2009 tarent GmbH
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License,version 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 *
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'taBoMS'
 * Signature of Elmar Geese, 21 April 2009
 * Elmar Geese, CEO tarent GmbH.
 */

package com.extjs.gxt.ui.client.messages;

public class MyMessages_ implements com.extjs.gxt.ui.client.messages.MyMessages {
  public java.lang.String messageBox_ok() {
    return "Ok";
  }
  
  public java.lang.String datePicker_minText() {
    return "This date is before the minimum date";
  }
  
  public java.lang.String dateField_disabledDaysText() {
    return "Disabled";
  }
  
  public java.lang.String datePicker_nextText() {
    return "Next Month";
  }
  
  public java.lang.String dateField_format() {
    return "m/d/y";
  }
  
  public java.lang.String gridView_columnsText() {
    return "Columns";
  }
  
  public java.lang.String loadMask_msg() {
    return "Loading...";
  }
  
  public java.lang.String pagingToolBar_displayMsg(int arg0,int arg1,int arg2) {
    return "Displaying " + arg0 + " - " + arg1 + " of " + arg2;
  }
  
  public java.lang.String textField_emptyText() {
    return "";
  }
  
  public java.lang.String pagingToolBar_firstText() {
    return "First Page";
  }
  
  public java.lang.String field_invalidText() {
    return "The value in this field is invalid";
  }
  
  public java.lang.String grid_ddText(int arg0) {
    return "" + arg0 + " selected row(s)";
  }
  
  public java.lang.String pagingToolBar_afterPageText(int arg0) {
    return "of " + arg0;
  }
  
  public java.lang.String pagingToolBar_lastText() {
    return "Last Page";
  }
  
  public java.lang.String pagingToolBar_emptyMsg() {
    return "No data to display";
  }
  
  public java.lang.String datePicker_todayTip(java.lang.String arg0) {
    return arg0;
  }
  
  public java.lang.String datePicker_prevText() {
    return "Previous Month";
  }
  
  public java.lang.String updateManager_indicatorText() {
    return "<div class=\"loading-indicator\">Loading...</div>";
  }
  
  public java.lang.String textField_regexText() {
    return "";
  }
  
  public java.lang.String propertyColumnModel_nameText() {
    return "Name";
  }
  
  public java.lang.String dateField_invalidText(java.lang.String arg0,java.lang.String arg1) {
    return arg0 + " is not a valid date - it must be in the format " + arg1;
  }
  
  public java.lang.String datePicker_disabledDatesText() {
    return "";
  }
  
  public java.lang.String datePicker_todayText() {
    return "Today";
  }
  
  public java.lang.String pagingToolBar_beforePageText() {
    return "Page";
  }
  
  public java.lang.String dateField_altFormats() {
    return "m/d/Y|m-d-y|m-d-Y|m/d|m-d|md|mdy|mdY|d|Y-m-d";
  }
  
  public java.lang.String datePicker_cancelText() {
    return "Cancel";
  }
  
  public java.lang.String datePicker_okText() {
    return "&#160;OK&#160;";
  }
  
  public java.lang.String dateField_minText(java.lang.String arg0) {
    return "The date in this field must be after " + arg0;
  }
  
  public java.lang.String groupingView_showGroupsText() {
    return "Show in Groups";
  }
  
  public java.lang.String messageBox_yes() {
    return "Yes";
  }
  
  public java.lang.String numberField_nanText(java.lang.String arg0) {
    return arg0 + " is not a valid number";
  }
  
  public java.lang.String groupingView_emptyGroupText() {
    return "(None)";
  }
  
  public java.lang.String messageBox_no() {
    return "No";
  }
  
  public java.lang.String pagingToolBar_prevText() {
    return "Previous Page";
  }
  
  public java.lang.String textField_blankText() {
    return "This field is required";
  }
  
  public java.lang.String datePicker_maxText() {
    return "This date is after the maximum date";
  }
  
  public java.lang.String borderLayout_splitTip() {
    return "Drag to resize.";
  }
  
  public java.lang.String messageBox_close() {
    return "Close";
  }
  
  public java.lang.String dateField_maxText(java.lang.String arg0) {
    return "The date in this field must be before " + arg0;
  }
  
  public java.lang.String pagingToolBar_nextText() {
    return "Next Page";
  }
  
  public java.lang.String datePicker_monthYearText() {
    return "Choose a month";
  }
  
  public java.lang.String pagingToolBar_refreshText() {
    return "Refresh";
  }
  
  public java.lang.String numberField_minText(double arg0) {
    return "The minimum value for this field is " + arg0;
  }
  
  public java.lang.String gridView_sortDescText() {
    return "Sort Descending";
  }
  
  public java.lang.String datePicker_disabledDaysText() {
    return "";
  }
  
  public java.lang.String propertyColumnModel_valueText() {
    return "Value";
  }
  
  public java.lang.String tabPanelItem_closeText() {
    return "Close this tab";
  }
  
  public java.lang.String textField_minLengthText(int arg0) {
    return "The minimum length for this field is " + arg0;
  }
  
  public java.lang.String dateField_disabledDatesText() {
    return "Disabled";
  }
  
  public java.lang.String comboBox_loading() {
    return "Loading...";
  }
  
  public java.lang.String borderLayout_collapsibleSplitTip() {
    return "Drag to resize. Double click to hide.";
  }
  
  public java.lang.String textField_maxLengthText(int arg0) {
    return "The maximum length for this field is " + arg0;
  }
  
  public java.lang.String themeSelector_blueTheme() {
    return "Blue Theme";
  }
  
  public java.lang.String themeSelector_grayTheme() {
    return "Gray Theme";
  }
  
  public java.lang.String messageBox_cancel() {
    return "Cancel";
  }
  
  public java.lang.String groupingView_groupByText() {
    return "Group By This Field";
  }
  
  public java.lang.String datePicker_startDay() {
    return "0";
  }
  
  public java.lang.String gridView_sortAscText() {
    return "Sort Ascending";
  }
  
  public java.lang.String numberField_maxText(double arg0) {
    return "The maximum value for this field is " + arg0;
  }
  
  }
