/*
 * taBoMS,
 * tarent Book Management System user interface
 * Copyright (C) 2000-2009 tarent GmbH
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License,version 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 *
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'taBoMS'
 * Signature of Elmar Geese, 21 April 2009
 * Elmar Geese, CEO tarent GmbH.
 */

/**
 * 
 */
package de.tarent.taBoMS.client.i18n;

import com.google.gwt.i18n.client.Constants;

/**
 * @author Frederic Eßer
 *
 */
public interface TaBoMSI18N extends Constants
{
	String newBook();
	String langPicDE();
	String langPicEN();
	String windowHeader();
	String windowBookInfo();
	String isbn();
	String title();
	String subject();
	String launch();
	String author();
	String authorName();
	String publisher();
	String publisherName();
	String publisherHomepage();
	String general();
	String location();
	String description();
	String owner();
	String saveButton();
	String preview();
}
