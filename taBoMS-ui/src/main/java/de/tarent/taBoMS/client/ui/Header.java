/*
 * taBoMS,
 * tarent Book Management System user interface
 * Copyright (C) 2000-2009 tarent GmbH
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License,version 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 *
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'taBoMS'
 * Signature of Elmar Geese, 21 April 2009
 * Elmar Geese, CEO tarent GmbH.
 */

/**
 * 
 */
package de.tarent.taBoMS.client.ui;

import com.extjs.gxt.ui.client.widget.HorizontalPanel;
import com.extjs.gxt.ui.client.widget.Html;
import com.google.gwt.core.client.GWT;
import com.google.gwt.user.client.ui.Image;
import com.google.gwt.user.client.ui.Label;

import de.tarent.taBoMS.client.i18n.TaBoMSI18N;

/**
 * @author Frederic Eßer
 *
 */
public class Header
{
	private Image logo = new Image("./img/logo.png");
	private Image langDE = new Image("./img/de.jpg");
	private Image langEN = new Image("./img/en.jpg");
	
	TaBoMSI18N i18n = (TaBoMSI18N) GWT.create(TaBoMSI18N.class);
	
	public Header(HorizontalPanel header)
	{
		logo.addStyleName("logo");
		
		langDE.addStyleName("languagePic");
		langEN.addStyleName("languagePic");
		
		langDE.setTitle(i18n.langPicDE()); 
		langEN.setTitle(i18n.langPicEN()); 
		
		Html de = new Html("<a href=\"" + GWT.getHostPageBaseURL() 
				+ "UserInterface.html?locale=de\">"
				+ langDE + "</a>");
		
		Html en = new Html("<a href=\"" + GWT.getHostPageBaseURL() 
				+ "UserInterface.html?locale=en\">"
				+ langEN + "</a>");
		
		header.setStyleName("header");
		
		Label room = new Label();
		room.addStyleName("room");
		
		header.add(logo);
		header.add(room);
		header.add(de);
		header.add(en);
	}
}