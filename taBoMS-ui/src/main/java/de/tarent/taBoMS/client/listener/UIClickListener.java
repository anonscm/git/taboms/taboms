/*
 * taBoMS,
 * tarent Book Management System user interface
 * Copyright (C) 2000-2009 tarent GmbH
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License,version 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 *
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'taBoMS'
 * Signature of Elmar Geese, 21 April 2009
 * Elmar Geese, CEO tarent GmbH.
 */

/**
 * 
 */
package de.tarent.taBoMS.client.listener;

import com.google.gwt.user.client.ui.ClickListener;
import com.google.gwt.user.client.ui.Widget;

import de.tarent.taBoMS.client.ui.ImageFlow;
import de.tarent.taBoMS.client.ui.UIComponents;

/**
 * @author Frederic Eßer
 *
 */
public class UIClickListener implements ClickListener
{
	int pos = 0;

	/**
	 * @param i
	 */
	public UIClickListener(int i)
	{
		this.pos = i;
	}

	/**
	 * @see com.google.gwt.user.client.ui.ClickListener#onClick(com.google.gwt.user.client.ui.Widget)
	 */
	public void onClick(Widget sender)
	{
		int position = ImageFlow.getPosition();
		int size = ImageFlow.getImages().size();
		
		switch (pos) {
			case 1:	// leftSide image
				if(position-1 > 0)
				{
					ImageFlow.setPosition(position-1);
					UIComponents.contentPanel.clear();
					ImageFlow.displayImages(UIComponents.contentPanel);
				}
				break;
			case 2: // front image
				ImageFlow.setInformation(0);
				ImageFlow.bookInformation.show();
				break;
			case 3:	// rightSide image
				if(position+1 < size-1)
				{
					ImageFlow.setPosition(position+1);
					UIComponents.contentPanel.clear();
					ImageFlow.displayImages(UIComponents.contentPanel);
				}
				break;
		}
	}

}