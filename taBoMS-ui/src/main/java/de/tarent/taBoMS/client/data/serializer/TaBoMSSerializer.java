/**
 * 
 */
package de.tarent.taBoMS.client.data.serializer;

import java.util.List;
import java.util.Map;
import java.util.Set;

import com.google.gwt.core.client.GWT;
import com.google.gwt.xml.client.Element;
import com.google.gwt.xml.client.Node;

import de.tarent.gwt.ws.client.soap.SoapFactory;
import de.tarent.gwt.ws.client.types.Entry;
import de.tarent.gwt.ws.client.types.Serializer;
import de.tarent.gwt.ws.client.types.SerializerUtils;
import de.tarent.taBoMS.client.data.File;
import de.tarent.taBoMS.client.data.Role;

/**
 * @author Frederic Eßer
 *
 */
public class TaBoMSSerializer implements Serializer {

	public Node serialize(Node rootNode, Object object)
    {
        if (object instanceof de.tarent.gwt.ws.client.soap.Node && ((de.tarent.gwt.ws.client.soap.Node)object).getType()==de.tarent.gwt.ws.client.soap.Node.OBJECT_NODE)
        {
            // is an SOAP implementation object node
            object = ((de.tarent.gwt.ws.client.soap.Node)object).getValue();
        }
        
        if (object instanceof List)
        {
            List elementList = (List)object;
            Element thisListElement = null;
            if (elementList.size() > 0) {
	            if (elementList.get(0) instanceof Role)
	            	 thisListElement = rootNode.getOwnerDocument().createElement("roles");
	            else if (elementList.get(0) instanceof File)
	            	thisListElement = rootNode.getOwnerDocument().createElement("anlagen");
	            else
	            	 thisListElement = rootNode.getOwnerDocument().createElement("roleIds");
            }
            
            for (int i=0; i<elementList.size(); ++i)
            {
            	if (elementList.get(i) instanceof Entry) 
            	{
            		Entry thisEntry = (Entry)elementList.get(i);
            		thisListElement.appendChild(serialize(thisListElement, thisEntry));
            	} 
            	else if (elementList.get(i) instanceof String)
            	{
            		String thisEntry = (String) elementList.get(i);
            		Entry entry = new Entry("roleId", thisEntry);
            		thisListElement.appendChild(serialize(thisListElement, entry));
            	} 
            	else if (elementList.get(i) instanceof Role)
            	{
            		Role thisEntry = (Role) elementList.get(i);
            		Entry entry = new Entry("role", thisEntry);
            		Node roleNode = thisListElement.appendChild(serialize(thisListElement, entry));
            	}
            	else if (elementList.get(i) instanceof File)
            	{
            		File thisEntry = (File) elementList.get(i);
            		Entry entry = new Entry("dateiAnlage", thisEntry);
            		Node roleNode = thisListElement.appendChild(serialize(thisListElement, entry));
            	}
            }
            
            return thisListElement;
        }
        else if (object instanceof Entry)
        {
            Entry entry = (Entry) object;
            Element thisElement = rootNode.getOwnerDocument().createElement(entry.getKey());
            if (((Entry) object).getValue() instanceof Role)
            {
            	final Role role = (Role) ((Entry) object).getValue();
            	final Map<String, Object> childEntries = role.getProperties();
            	final Set<String> keySet = childEntries.keySet();
        		for (String keys: keySet)
        		{
        			// Small Roles may have no forst or lastname
        			if (childEntries.get(keys) != null)
        			{
        				thisElement.appendChild(serialize(thisElement, new Entry(keys, childEntries.get(keys))));
        			}
//                    thisElement.appendChild(SoapFactory.getSerializerSettings().getSerializer(GWT.getTypeName(entry.getValue())).serialize(thisElement, childEntries.get(keys)));
//        			thisElement = (Element) childEntries.get(keys);
//        			thisElement.appendChild(serialize(SoapFactory.getSerializerSettings().getSerializer(GWT.getTypeName(entry.getValue())).serialize(keys, childEntries.get(keys))));
        		}
        		return thisElement;
            } else if (((Entry) object).getValue() instanceof File)
            {
            	final File fileAnlage = (File) ((Entry) object).getValue();
            	final Map<String, Object> childEntries = fileAnlage.getProperties();
            	final Set<String> keySet = childEntries.keySet();
        		for (String keys: keySet)
        		{
        			// Small Roles may have no forst or lastname
        			if (childEntries.get(keys) != null)
        			{
        				thisElement.appendChild(serialize(thisElement, new Entry(keys, childEntries.get(keys))));
        			}
        		}
        		return thisElement;

            }
            thisElement.appendChild(SoapFactory.getSerializerSettings().getSerializer(GWT.getTypeName(entry.getValue())).serialize(thisElement, entry.getValue()));
            return thisElement;
        }
        else if (object instanceof Role)
        {
            Element thisElement = rootNode.getOwnerDocument().createElement("role");
         	final Role role = (Role) object;
        	final Map<String, Object> childEntries = role.getProperties();
        	final Set<String> keySet = childEntries.keySet();
    		for (String keys: keySet)
    		{
    			// Small Roles may have no forst or lastname
    			if (childEntries.get(keys) != null)
    			{
    				thisElement.appendChild(serialize(thisElement, new Entry(keys, childEntries.get(keys))));
    			}
//                thisElement.appendChild(SoapFactory.getSerializerSettings().getSerializer(GWT.getTypeName(entry.getValue())).serialize(thisElement, childEntries.get(keys)));
//    			thisElement = (Element) childEntries.get(keys);
//    			thisElement.appendChild(serialize(SoapFactory.getSerializerSettings().getSerializer(GWT.getTypeName(entry.getValue())).serialize(keys, childEntries.get(keys))));
    		}
    		return thisElement;
        }
        else
        {
            return SerializerUtils.baselevelSerialize(rootNode, object);
        }
    }


}