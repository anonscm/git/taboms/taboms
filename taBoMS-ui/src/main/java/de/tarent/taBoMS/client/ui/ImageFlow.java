/*
 * taBoMS,
 * tarent Book Management System user interface
 * Copyright (C) 2000-2009 tarent GmbH
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License,version 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 *
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'taBoMS'
 * Signature of Elmar Geese, 21 April 2009
 * Elmar Geese, CEO tarent GmbH.
 */

package de.tarent.taBoMS.client.ui;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import com.extjs.gxt.ui.client.event.BaseEvent;
import com.extjs.gxt.ui.client.event.ComponentEvent;
import com.extjs.gxt.ui.client.event.Listener;
import com.extjs.gxt.ui.client.event.SelectionListener;
import com.extjs.gxt.ui.client.widget.HorizontalPanel;
import com.extjs.gxt.ui.client.widget.VerticalPanel;
import com.extjs.gxt.ui.client.widget.Window;
import com.extjs.gxt.ui.client.widget.button.Button;
import com.extjs.gxt.ui.client.widget.form.DateField;
import com.extjs.gxt.ui.client.widget.form.FormPanel;
import com.extjs.gxt.ui.client.widget.form.TextArea;
import com.extjs.gxt.ui.client.widget.form.TextField;
import com.extjs.gxt.ui.client.widget.layout.FlowLayout;
import com.google.gwt.core.client.GWT;
import com.google.gwt.user.client.Event;
import com.google.gwt.user.client.ui.ClickListener;
import com.google.gwt.user.client.ui.FocusPanel;
import com.google.gwt.user.client.ui.Image;
import com.google.gwt.user.client.ui.Widget;

import de.tarent.gwt.ws.client.SoapRequestCallback;
import de.tarent.gwt.ws.client.types.Entry;
import de.tarent.taBoMS.client.communication.ServerCommunication;
import de.tarent.taBoMS.client.communication.TaBoMSRequestCallback;
import de.tarent.taBoMS.client.i18n.TaBoMSI18N;
import de.tarent.taBoMS.client.listener.UIClickListener;

/**
 * @author Frederic Eßer
 *
 */
public class ImageFlow
{
	private static ArrayList<Image> images;
	private static int position;
	
	private Image sideImage = new Image("./img/side.png");
	
	static Image leftSide = new Image();
	static Image front = new Image();
	static Image rightSide = new Image();
	
	static UIClickListener leftSideClickListener = new UIClickListener(1);
	static UIClickListener frontClickListener = new UIClickListener(2);
	static UIClickListener rightSideClickListener = new UIClickListener(3);
	
	public static Window bookInformation;

	public static TextField<String> isbn = new TextField<String>();
	public static TextField<String> title =  new TextField<String>();
//	public static TextField<String> subject = new TextField<String>();
	public static DateField launch = new DateField();
	public static TextField<String> authorName = new TextField<String>();
	public static TextField<String> publisherName = new TextField<String>();
	public static TextField<String> publisherHomepage = new TextField<String>();
	public static TextField<String> location = new TextField<String>();
	public static TextArea description = new TextArea();
	public static TextField<String> owner = new TextField<String>();
	
	public static Button saveButton = new Button();
	public static Image currentSelected = new Image(null);
	
	static TaBoMSI18N i18n = (TaBoMSI18N) GWT.create(TaBoMSI18N.class);
	
	/**
	 * constructor to create and add the slide show to UI
	 * 
	 * @param contentPanel
	 */
	public ImageFlow(FocusPanel contentPanel)
	{
		setImages(new ArrayList<Image>());
		
		images.add(sideImage); // Transparent picture at the first position
		images.add(new Image("./img/51O7HqUIGuL.jpg")); // \
		images.add(new Image("./img/51WVE-XD8-L.jpg")); //  > replace by loop do initial the images
		images.add(new Image("./img/41F47Q59QPL.jpg")); // /
		images.add(sideImage); // Transparent picture at the last position
		
		setPosition((getImages().size()) / 2); // get the central position of the Images

		displayImages(contentPanel);

		ImageFlow.displayInformation();
	}
	
	/**
	 * function to display the images as slide show
	 * 
	 * @param focusPanel
	 */
	public static void displayImages(FocusPanel focusPanel)
	{	
		// remove ClickListener
		if(leftSideClickListener != null
				|| frontClickListener != null
				|| rightSideClickListener != null)
		{
			leftSide.removeClickListener(leftSideClickListener);
			front.removeClickListener(frontClickListener);
			rightSide.removeClickListener(rightSideClickListener);
		}
		
		// load images
		leftSide = getImages().get(position-1);
		front = getImages().get(position);
		rightSide = getImages().get(position+1);
		
		// register ClickListener
		leftSide.addClickListener(leftSideClickListener);
		front.addClickListener(frontClickListener);
		rightSide.addClickListener(rightSideClickListener);
		
		// set size of images
		leftSide.setSize("191px", "250px");
		front.setSize("229px", "300px");
		rightSide.setSize("191px", "250px");
		
		// set style
		leftSide.addStyleName("imgSlide");
		front.addStyleName("imgSlide");
		rightSide.addStyleName("imgSlide");
		
		// create Panel
		HorizontalPanel imageFlowPanel = new HorizontalPanel();
		
		// images -> Panel
		imageFlowPanel.add(leftSide);
		imageFlowPanel.add(front);
		imageFlowPanel.add(rightSide);
		
		// add to contentPanel
		focusPanel.add(imageFlowPanel);
	}
	
	/**
	 * function to display the information
	 * fired by click on a book picture or "new book" button
	 */
	public static void displayInformation()
	{
		bookInformation  = new Window();
		
		// configure Window
		bookInformation.setWidth("600px");
		bookInformation.setHeight("584px");
		bookInformation.setHeading(i18n.windowHeader());
		bookInformation.setStyleName("bookInformation");
		bookInformation.setModal(true);
		bookInformation.setLayout(new FlowLayout());
		
		VerticalPanel windowBookVerticalPanel = new VerticalPanel();
		
		HorizontalPanel bookInfoHorizontalPanel = new HorizontalPanel();
		
		VerticalPanel bookInformationVerticalPanel = new VerticalPanel();
		
		FormPanel content = new FormPanel();
		content.setHeading(i18n.windowBookInfo());	// Book-Information
		content.setFrame(true);
		content.setWidth(404);
		
		isbn.setFieldLabel(i18n.isbn());
		title.setFieldLabel(i18n.title());
//		subject.setFieldLabel(i18n.subject());
		launch.setFieldLabel(i18n.launch());
		
		content.add(isbn);
		content.add(title);
//		content.add(subject);
		content.add(launch);
		
		bookInformationVerticalPanel.add(content);
		
		content = new FormPanel();
		content.setHeading(i18n.author());			// Author-Information
		content.setFrame(true);
		
		authorName.setFieldLabel(i18n.authorName());
		
		content.add(authorName);
		
		bookInformationVerticalPanel.add(content);
		
		bookInfoHorizontalPanel.add(bookInformationVerticalPanel);
		
		content = new FormPanel();
		content.setHeading(i18n.preview());			// Book-Preview
		content.setFrame(true);
		
		currentSelected = new Image("./img/default.jpg");
		currentSelected.setHeight("197");
		currentSelected.setWidth("150");
		
		content.add(currentSelected);
		
		bookInfoHorizontalPanel.add(content);
		
		windowBookVerticalPanel.add(bookInfoHorizontalPanel);
		
		content = new FormPanel();
		content.setHeading(i18n.publisher());		// Publisher-Information
		content.setFrame(true);
		
		publisherName.setFieldLabel(i18n.publisherName());
		publisherHomepage.setFieldLabel(i18n.publisherHomepage());
		
		content.add(publisherName);
		content.add(publisherHomepage);
		
		windowBookVerticalPanel.add(content);
		
		content = new FormPanel();
		content.setHeading(i18n.general());			// General-Information
		content.setFrame(true);
		
		location.setFieldLabel(i18n.location());
		owner.setFieldLabel(i18n.owner());
		description.setFieldLabel(i18n.description());
		
		content.add(location);
		content.add(owner);
		content.add(description);
		
		saveButton.setText(i18n.saveButton());

		
		content.add(saveButton);
		
		//*****************************************
		// just a test button to test a request
		com.google.gwt.user.client.ui.Button test = new com.google.gwt.user.client.ui.Button("test", new ClickListener() {

			public void onClick(Widget arg0) {
				TaBoMSRequestCallback callback = new TaBoMSRequestCallback();
				List<Entry> parameters = new LinkedList<Entry>();
				final String isbn = "3142250425";
//				final String isbn = "";
				parameters.add(new Entry("code", isbn));
				
				ServerCommunication comm =  new ServerCommunication();
				
				comm.request(callback, "getAmazonInfo", parameters);
				
				
				GWT.log("" + callback, null);
//				do {
//					GWT.log("nix", null);
//				} while (callback == null);
			}});
		
		content.add(test);
		//*****************************************
		
		windowBookVerticalPanel.add(content);
		
		bookInformation.add(windowBookVerticalPanel);
	}
	
	/**
	 * function to set the information in the information window
	 * state:<br>
	 * 0 -> opened by click on a book<br>
	 * 1 -> opened by click on the button "new book"
	 * 
	 * @param state
	 */
	public static void setInformation(int state)
	{
		if(state == 0)		// fired from click on a book
		{
			isbn.setValue("book");
			currentSelected.setUrl(front.getUrl());
		} else if(state == 1) // new book
		{
			isbn.setValue("");
			title.setValue("");
			currentSelected.setUrl("./img/default.jpg");
//			subject.setValue("");
			launch.setValue(null);
			authorName.setValue("");
			publisherName.setValue("");
			publisherHomepage.setValue("");
			location.setValue("");
			description.setValue("");
			owner.setValue("");
		}
	}

	/**
	 * @param images the images to set
	 */
	public static void setImages(ArrayList<Image> img)
	{
		images = img;
	}

	/**
	 * @return the images
	 */
	public static ArrayList<Image> getImages() 
	{
		return images;
	}

	/**
	 * @param position the position to set
	 */
	public static void setPosition(int pos)
	{
		position = pos;
	}

	/**
	 * @return the position
	 */
	public static int getPosition()
	{
		return position;
	}
}