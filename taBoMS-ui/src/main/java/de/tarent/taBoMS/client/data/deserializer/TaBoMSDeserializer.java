/**
 * 
 */
package de.tarent.taBoMS.client.data.deserializer;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.google.gwt.core.client.GWT;

import de.tarent.gwt.ws.client.soap.Body;
import de.tarent.gwt.ws.client.soap.Element;
import de.tarent.gwt.ws.client.soap.SoapMessage;
import de.tarent.gwt.ws.client.types.Entry;
import de.tarent.taBoMS.client.data.List.AuthorList;
import de.tarent.taBoMS.client.data.WSserver.WSAuthor;

/**
 * @author Frederic Eßer
 *
 */
public class TaBoMSDeserializer
{
	static Map<String, Object> soapMessageMap; 
	
	@SuppressWarnings("unchecked")
	public static Object deserialize(SoapMessage soapMessage)
	{
		if(soapMessage == null)
		{
			return null;
		}
		
		List resultList = new ArrayList();
		
		soapMessageMap = new HashMap<String, Object>();
		final Body body = soapMessage.getBody();
		final Element child = (Element) body.getChilds().get(1);
		final ArrayList bookList = (ArrayList) child.getValue();
		final Entry wsBookList = (Entry) bookList.get(0);
		final ArrayList authors = (ArrayList) wsBookList.getValue();
		
//		AuthorList authorList = new AuthorList();
//		for(int i = 0; i < )
		// TODO read soap structure and write into WSAuthor / WSPublisher objects 
		// save them into AuthorList and PublisherList. 
		/*
		 * [WSBOOK:
		 * 	 [
		 * 	   [author:
		 *       [
		 *         [WSAuthor:
		 *           [
		 *             [authorID:0], [name:Heinrich Hübscher]
		 *           ]
		 *         ], 
		 *         [WSAuthor:
		 *           [
		 *             [authorID:0], [name:Hans-Joachim Petersen]
		 *           ]
		 *         ], 
		 *         [WSAuthor:
		 *           [
		 *             [authorID:0], [name:Carsten Rathgeber]
		 *           ]
		 *         ], 
		 *         [WSAuthor:
		 *           [
		 *             [authorID:0], [name:Klaus Richter]
		 *           ]
		 *         ], 
		 *         [WSAuthor:
		 *           [
		 *             [authorID:0], [name:Dirk Scharf]
		 *           ]
		 *         ]
		 *       ]
		 *     ], 
		 *     [bookID:0], [description:IT-Handbuch für Systemelektroniker/-in, Fachinformatiker/-in Tabellenbuch. SB], 
		 *     [image:510unwtN%2BxL._SL500_.jpg], 
		 *     [isbn:3142250425], 
		 *     [launch:2009-01-31T23:00:00.000Z], 
		 *     [publisher:
		 *       [
		 *         [WSPublisher:
		 *           [
		 *             [name:Westermann Berufsbildung], [publisherID:0]
		 *           ]
		 *         ]
		 *       ]
		 *     ], 
		 *     [title:IT-Handbuch für Systemelektroniker/-in, Fachinformatiker/-in: Tabellenbuch]
		 *   ]
		 * ]
		 */
		
		
		GWT.log(wsBookList.toString(),null);
//		List<Entry> book = childs.get(1).
		
		return soapMessage;
	}
}
