/**
 * 
 */
package de.tarent.taBoMS.client.communication;

import de.tarent.gwt.ws.client.SoapRequestCallback;
import de.tarent.gwt.ws.client.soap.SoapMessage;
import de.tarent.taBoMS.client.data.deserializer.TaBoMSDeserializer;

/**
 * @author Frederic Eßer
 *
 */
public class TaBoMSRequestCallback implements SoapRequestCallback
{

	public void onError(Throwable exception) 
	{
		ServerCommunication.getInstance().alertCommunicationFailure();
	}

	public void onResponseReceived(SoapMessage response)
	{
		Object o = TaBoMSDeserializer.deserialize(response);
		
		this.callback(o);
	}
	
	public void callback( Object o ) {
	}

}
