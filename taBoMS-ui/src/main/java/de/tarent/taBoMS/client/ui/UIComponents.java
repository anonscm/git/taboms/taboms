/*
 * taBoMS,
 * tarent Book Management System user interface
 * Copyright (C) 2000-2009 tarent GmbH
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License,version 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 *
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'taBoMS'
 * Signature of Elmar Geese, 21 April 2009
 * Elmar Geese, CEO tarent GmbH.
 */

package de.tarent.taBoMS.client.ui;

import com.extjs.gxt.ui.client.event.ComponentEvent;
import com.extjs.gxt.ui.client.event.SelectionListener;
import com.extjs.gxt.ui.client.widget.ContentPanel;
import com.extjs.gxt.ui.client.widget.HorizontalPanel;
import com.extjs.gxt.ui.client.widget.VerticalPanel;
import com.extjs.gxt.ui.client.widget.button.Button;
import com.extjs.gxt.ui.client.widget.layout.CenterLayout;
import com.google.gwt.core.client.GWT;
import com.google.gwt.user.client.ui.FocusPanel;

import de.tarent.taBoMS.client.UserInterface;
import de.tarent.taBoMS.client.i18n.TaBoMSI18N;

/**
 * @author Frederic Eßer
 *
 */
public class UIComponents
{
	public static FocusPanel contentPanel;
	public static HorizontalPanel header;
	TaBoMSI18N i18n = (TaBoMSI18N) GWT.create(TaBoMSI18N.class);
	
	public UIComponents()
	{
		VerticalPanel design = new VerticalPanel();
		design.setStyleName("design");
		
		header = new HorizontalPanel();
		new Header(header);
		
		contentPanel = new FocusPanel();
//		contentPanel.setFocus(true);
		contentPanel.addStyleName("contentPanel");
		new ImageFlow(contentPanel);
		
		ContentPanel slideShowWrapper = new ContentPanel();
		slideShowWrapper.setLayout(new CenterLayout());
		
		slideShowWrapper.add(contentPanel);
		slideShowWrapper.setHeaderVisible(false);
		slideShowWrapper.setBodyBorder(false);
		
		SelectionListener<ComponentEvent> selectionListener = new SelectionListener<ComponentEvent>(){

			@Override
			public void componentSelected(ComponentEvent ce) {
				ImageFlow.setInformation(1);
				ImageFlow.bookInformation.show();
			}
		};
		
		Button newBook = new Button(i18n.newBook(), selectionListener);
		newBook.addStyleName("newBook");
		
		design.add(header);
		design.add(slideShowWrapper);
		design.add(newBook);
		
		UserInterface.contentArea.add(design);
	}
}