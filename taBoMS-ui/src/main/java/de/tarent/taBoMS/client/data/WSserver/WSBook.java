/*
 * tarent Book Management System - Server,
 * tarent Book Management System - Server
 * Copyright (C) 2000-2009 tarent GmbH
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License,version 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 *
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'tarent Book Management System - Server'
 * Signature of Elmar Geese, 21 April 2009
 * Elmar Geese, CEO tarent GmbH.
 */

package de.tarent.taBoMS.client.data.WSserver;

import java.util.Date;

import de.tarent.taBoMS.client.data.List.AuthorList;
import de.tarent.taBoMS.client.data.List.PublisherList;

/**
 * @author Frederic Eßer
 *
 */
public class WSBook
{
	private long bookID;
	private String title;
	private String location;
	private String isbn;
	private String owner;
	private Date launch;
	private String subject;
	private String image;
	private String description;
	
	private AuthorList author = new AuthorList();
	private PublisherList publisher = new PublisherList();
	
	/**
	 * 
	 */
	public WSBook()
	{
		
	}

	/**
	 * @param bookID the bookID to set
	 */
	public void setBookID(long bookID) {
		this.bookID = bookID;
	}

	/**
	 * @return the bookID
	 */
	public long getBookID() {
		return bookID;
	}

	/**
	 * @param title the title to set
	 */
	public void setTitle(String title) {
		this.title = title;
	}

	/**
	 * @return the title
	 */
	public String getTitle() {
		return title;
	}

	/**
	 * @param location the location to set
	 */
	public void setLocation(String location) {
		this.location = location;
	}

	/**
	 * @return the location
	 */
	public String getLocation() {
		return location;
	}

	/**
	 * @param isbn the isbn to set
	 */
	public void setIsbn(String isbn) {
		this.isbn = isbn;
	}

	/**
	 * @return the isbn
	 */
	public String getIsbn() {
		return isbn;
	}

	/**
	 * @param owner the owner to set
	 */
	public void setOwner(String owner) {
		this.owner = owner;
	}

	/**
	 * @return the owner
	 */
	public String getOwner() {
		return owner;
	}

	/**
	 * @param launch the launch to set
	 */
	public void setLaunch(Date launch) {
		this.launch = launch;
	}

	/**
	 * @return the launch
	 */
	public Date getLaunch() {
		return launch;
	}

	/**
	 * @param subject the subject to set
	 */
	public void setSubject(String subject) {
		this.subject = subject;
	}

	/**
	 * @return the subject
	 */
	public String getSubject() {
		return subject;
	}

	/**
	 * @param image the image to set
	 */
	public void setImage(String image) {
		this.image = image;
	}

	/**
	 * @return the image
	 */
	public String getImage() {
		return image;
	}

	/**
	 * @param author the author to set
	 */
	public void setAuthor(AuthorList author) {
		this.author = author;
	}

	/**
	 * @return the author
	 */
	public AuthorList getAuthor() {
		return author;
	}

	/**
	 * @param publisher the publisher to set
	 */
	public void setPublisher(PublisherList publisher) {
		this.publisher = publisher;
	}

	/**
	 * @return the publisher
	 */
	public PublisherList getPublisher() {
		return publisher;
	}

	/**
	 * @param description the description to set
	 */
	public void setDescription(String description) {
		this.description = description;
	}

	/**
	 * @return the description
	 */
	public String getDescription() {
		return description;
	}
}
