package de.tarent.taBoMS.client.data;

import com.extjs.gxt.ui.client.data.BaseModel;

/** represents a file
 * 
 * @author Martin Pelzer, tarent GmbH
 *
 */
public class File extends BaseModel {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public String getFileName() {
		return get("fileName");
	}
	
	public void setFileName(String fileName) {
		set("fileName", fileName);
	}
	
	public String getHash() {
		return get("hash");
	}
	
	public void setHash(String hash) {
		set("hash", hash);
	}
	
}
