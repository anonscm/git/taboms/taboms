/**
 * 
 */
package de.tarent.taBoMS.client.data.serializer;

import de.tarent.gwt.ws.client.types.Deserializer;
import de.tarent.gwt.ws.client.types.MapDeserializer;
import de.tarent.gwt.ws.client.types.Serializer;
import de.tarent.gwt.ws.client.types.SerializerSettings;

/**
 * @author Frederic Eßer
 *
 */
public class TaBoMSSerializerSettings implements SerializerSettings
{	

	public Deserializer getDeserializer(String type) {
		
		return new MapDeserializer();
	}

	public Serializer getSerializer(String classname) {
		if(classname == null)
		{
			return new TaBoMSSerializer();
		}
		
		if(classname.equals("java.util.HashMap"))
		{
			return new HashMapSerializer();
		}
		return new TaBoMSSerializer();
	}

}
