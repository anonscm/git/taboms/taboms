/*
 * taBoMS,
 * tarent Book Management System user interface
 * Copyright (C) 2000-2009 tarent GmbH
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License,version 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 *
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'taBoMS'
 * Signature of Elmar Geese, 21 April 2009
 * Elmar Geese, CEO tarent GmbH.
 */

/**
 * 
 */
package de.tarent.taBoMS.client.listener;

import com.extjs.gxt.ui.client.event.BaseEvent;
import com.extjs.gxt.ui.client.event.Listener;
import com.google.gwt.core.client.GWT;
import com.google.gwt.user.client.ui.Widget;

/**
 * @author Frederic Eßer
 *
 */
public class UIKeyListener implements /*Keyboard*/Listener
{

	public void onKeyDown(Widget sender, char keyCode, int modifiers) {
		System.out.println("down");
		GWT.log("down", null);
		
	}

	public void onKeyPress(Widget sender, char keyCode, int modifiers) {
		System.out.println("press");
		GWT.log("press", null);
	}

	public void onKeyUp(Widget sender, char keyCode, int modifiers) {
		System.out.println("up");
		GWT.log("up", null);
	}

	public void handleEvent(BaseEvent be) {
		System.out.println("press");
		GWT.log("press", null);
	}
}
