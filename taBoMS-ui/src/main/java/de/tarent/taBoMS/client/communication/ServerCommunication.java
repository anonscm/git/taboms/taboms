/**
 * 
 */
package de.tarent.taBoMS.client.communication;

import java.util.List;

import com.extjs.gxt.ui.client.widget.MessageBox;
import com.google.gwt.core.client.GWT;

import de.tarent.gwt.ws.client.SoapClient;
import de.tarent.gwt.ws.client.SoapRequestCallback;
import de.tarent.gwt.ws.client.soap.SoapException;
import de.tarent.gwt.ws.client.soap.SoapFactory;
import de.tarent.gwt.ws.client.types.Entry;
import de.tarent.taBoMS.client.data.serializer.TaBoMSSerializerSettings;

/**
 * @author Frederic Eßer
 *
 */
public class ServerCommunication
{
	private SoapClient soap = new SoapClient();
	
	private static ServerCommunication instance;
	
	String serverURL = "http://localhost:8090/taBoMS-server-1.0-SNAPSHOT/soap/taBoMS-server-1.0-SNAPSHOT";
	
	public ServerCommunication()
	{
		this.soap.setProxy(GWT.getHostPageBaseURL() + "/proxy");
		this.soap.setNamespace("http://schema.tarent.de/taboms/tabomsSchema");
		SoapFactory.setSerializerSettings(new TaBoMSSerializerSettings());
	}
	
	public void request(SoapRequestCallback callback, String operation, List<Entry> parameters)
	{
		try {
			this.soap.request(callback, this.serverURL, operation, parameters);
		} catch (SoapException e) {
			GWT.log("ERROR: no response", e);
			MessageBox.alert("Alert!", "No response recieved, please check your connection to server", null);
		}
	}
	

	public static ServerCommunication getInstance() 
	{
		if (instance == null)
			instance = new ServerCommunication();
		return instance;
	}
	
	public void alertCommunicationFailure() 
	{
		MessageBox.alert("Error", "error", null);
	}
	
	public void alertCommunicationFailure(String message) 
	{
		MessageBox.alert("Error", message, null);
	}

}
