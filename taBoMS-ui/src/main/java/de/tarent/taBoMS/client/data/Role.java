/**
 * 
 */
package de.tarent.taBoMS.client.data;

import java.util.Map;

import com.extjs.gxt.ui.client.data.BaseModel;

/** DTO containing basic information about a role
 * 
 * @author Martin Pelzer, tarent GmbH
 *
 */
public class Role extends BaseModel
{
	private static final long serialVersionUID = -7955085426805966040L;

	
	public Role() {
		super();
	}
	
	public Role(Map<String, Object> beanData) {
		super(beanData);
	}
	public void setTotalCount(String totalCount)
	{
		set("totalCount", totalCount);

	}
	public void setMailAddress(String mailAddress)
	{
		set("mailAddress", mailAddress);
	}
	
	public String getMailAddress()
	{
		return get("mailAddress");
	}
	
	public String getLastName() {
		return get("lastName");
	}
	
	public void setLastName(String lastName) {
		set("lastName", lastName);
	}
	
	public String getFirstName() {
		return get("firstName");
	}
	
	public void setFirstName(String firstName) {
		set("firstName", firstName);
	}
	
	public String getRole() {
		return get("roleId");
	}
	
	public void setRole(String role) {
		set("roleId", role);
	}
	
	public String getCn() {
		return get("cn");
	}
	
	public void setCn(String cn) {
		set("cn", cn);
	}
	
	public String getPersonalnummer() {
		return get("personalNummer");
	}
	
	public void setPersonalnummer(String personalnummer) {
		set("personalNummer", personalnummer);
	}
	
	public boolean isRegistratorRole() {
		Object o = get("registratorRole");
		if (o instanceof Boolean)
			return (Boolean) o;
		else
			return Boolean.valueOf((String) o);
	}

	public void setRegistratorRole(boolean isRegistratorRole) {
		set("registratorRole", isRegistratorRole);
	}
	
	public void setRegistratorRole(String isRegistratorRole) {
		set("registratorRole", isRegistratorRole);
	}

	public boolean isVSTRole() {
		Object o = get("VSTRole");
		if (o instanceof Boolean)
			return (Boolean) o;
		else
			return Boolean.valueOf((String) o);
	}

	public void setVSTRole(boolean isVSTRole) {
		set("VSTRole", isVSTRole);
	}
	
	public void setVSTRole(String isVSTRole) {
		set("VSTRole", isVSTRole);
	}
	
	
	public String toString() {
		return get("cn");
	}
	
}
