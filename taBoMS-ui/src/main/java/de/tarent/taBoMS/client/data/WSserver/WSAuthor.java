/*
 * tarent Book Management System - Server,
 * tarent Book Management System - Server
 * Copyright (C) 2000-2009 tarent GmbH
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License,version 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 *
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'tarent Book Management System - Server'
 * Signature of Elmar Geese, 21 April 2009
 * Elmar Geese, CEO tarent GmbH.
 */

/**
 * 
 */
package de.tarent.taBoMS.client.data.WSserver;

import java.util.ArrayList;
import java.util.Collection;

import de.tarent.taBoMS.client.data.List.BookList;

/**
 * @author Frederic Eßer
 *
 */
public class WSAuthor
{
	private long authorID;
	private String name;
	
	private Collection<BookList> books = new ArrayList<BookList>();

	/**
	 * @param authorID - long - the authorID to set
	 */
	public void setAuthorID(long authorID) {
		this.authorID = authorID;
	}

	/**
	 * @return the authorID
	 */
	public long getAuthorID() {
		return authorID;
	}

	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param books the books to set
	 */
	public void setBooks(Collection<BookList> books) {
		this.books = books;
	}

	/**
	 * @return the books
	 */
	public Collection<BookList> getBooks() {
		return books;
	}
}
