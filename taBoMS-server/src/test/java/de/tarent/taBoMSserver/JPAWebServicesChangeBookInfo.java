/*
 * tarent Book Management System - Server,
 * tarent Book Management System - Server
 * Copyright (C) 2000-2009 tarent GmbH
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License,version 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 *
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'tarent Book Management System - Server'
 * Signature of Elmar Geese, 21 April 2009
 * Elmar Geese, CEO tarent GmbH.
 */

package de.tarent.taBoMSserver;

import org.junit.Test;

import de.tarent.taBoMSServer.convert.WSAuthor;
import de.tarent.taBoMSServer.convert.WSBook;
import de.tarent.taBoMSServer.convert.WSPublisher;
import de.tarent.taBoMSServer.convert.lists.AuthorList;
import de.tarent.taBoMSServer.convert.lists.PublisherList;
import de.tarent.taBoMSServer.exception.TaBoMSException;
import de.tarent.taBoMSServer.worker.BookWorker;

/**
 * @author Frederic Eßer
 *
 */
public class JPAWebServicesChangeBookInfo
{
	@Test
	public void testChangeBookInfo()
	{
		AuthorList authorList = new AuthorList();
		// first author
		WSAuthor author = new WSAuthor();
		author.setAuthorID(1);
		author.setName("Heinrich HübschnerNEU");
		authorList.add(author);
		
		// second author
		author = new WSAuthor();
		author.setAuthorID(2);
		author.setName("Hans-Joachim PetersenNEU");
		authorList.add(author);
		
		// publisher
		PublisherList publisherList = new PublisherList();
		WSPublisher publisher = new WSPublisher();
		publisher.setPublisherID(6);
		publisher.setName("Westermann Berufsbildung");
		publisher.setHomepage(null);
		
		publisherList.add(publisher);
		
		WSBook wsBook = new WSBook();
		wsBook.setBookID(7);
		wsBook.setIsbn("3142250425");
		wsBook.setTitle("IT-Handbuch");
		wsBook.setImage("510unwtN%2BxL._SL500_.jpg");
		wsBook.setDescription("Es ist Gründlich, übersichtlich... und mehr");
		wsBook.setAuthor(authorList);
		wsBook.setPublisher(publisherList);
		wsBook.setLaunch(null);
		wsBook.setLocation("wo anders");
		wsBook.setOwner("ich nicht");
		
		BookWorker bookWorker = new BookWorker();
		
		// call task changeBookInfo
		try {
			bookWorker.changeBookInfo(null, wsBook);
		} catch (TaBoMSException e) {
			System.out.println("ERROR: " + e.errorCode);
			e.printStackTrace();
		}
	}
}
