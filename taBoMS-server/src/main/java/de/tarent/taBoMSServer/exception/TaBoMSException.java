/*
 * tarent Book Management System - Server,
 * tarent Book Management System - Server
 * Copyright (C) 2000-2009 tarent GmbH
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License,version 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 *
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'tarent Book Management System - Server'
 * Signature of Elmar Geese, 21 April 2009
 * Elmar Geese, CEO tarent GmbH.
 */

/**
 * 
 */
package de.tarent.taBoMSServer.exception;

/**
 * @author Frederic Eßer
 *
 */
public class TaBoMSException extends Exception
{

	/**
	 * 
	 */
	private static final long serialVersionUID = -8841696814065169454L;
	
	public int errorCode;
	
	public TaBoMSException()
	{
		
	}
	
	public TaBoMSException(int errorcode, String message)
	{
		super(message);
		this.errorCode = errorcode;		
	}

	public TaBoMSException(int errorcode, Exception exception)
	{
		super(exception);
		this.errorCode = errorcode;
	}
	
	public TaBoMSException(int errorcode, String message, Exception exception)
	{
		super(message, exception);
		this.errorCode = errorcode;
	}
}
