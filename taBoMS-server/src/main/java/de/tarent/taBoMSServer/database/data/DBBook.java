/*
 * tarent Book Management System - Server,
 * tarent Book Management System - Server
 * Copyright (C) 2000-2009 tarent GmbH
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License,version 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 *
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'tarent Book Management System - Server'
 * Signature of Elmar Geese, 21 April 2009
 * Elmar Geese, CEO tarent GmbH.
 */

package de.tarent.taBoMSServer.database.data;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.Iterator;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;

import de.tarent.taBoMSServer.convert.WSAuthor;
import de.tarent.taBoMSServer.convert.WSBook;
import de.tarent.taBoMSServer.convert.WSPublisher;
import de.tarent.taBoMSServer.convert.lists.AuthorList;
import de.tarent.taBoMSServer.convert.lists.PublisherList;

/**
 * @author Frederic Eßer
 *
 */
@Entity
public class DBBook
{
	private long bookID;
	private String title;
	private String location;
	private String isbn;
	private String owner;
	private Date launch;
	private String subject;
	private String image;
	private String description;
	
	private Collection<DBAuthor> author = new ArrayList<DBAuthor>();
	private Collection<DBPublisher> publisher = new ArrayList<DBPublisher>();
	
	/**
	 * 
	 */
	public DBBook()
	{
		
	}
	
	/**
	 * convert a wsBook into a book
	 * 
	 * @param book
	 */
	public DBBook(WSBook book)
	{
		this.setBookID(book.getBookID());
		this.setImage(book.getImage());
		this.setIsbn(book.getIsbn());
		this.setLaunch(book.getLaunch());
		this.setLocation(book.getLocation());
		this.setOwner(book.getOwner());
		this.setSubject(book.getSubject());
		this.setTitle(book.getTitle());
		this.setDescription(book.getDescription());
		
		AuthorList authors = book.getAuthor();
		Collection<DBAuthor> authorList = new ArrayList<DBAuthor>();
		
		for(Iterator<WSAuthor> itAuthor = authors.iterator(); itAuthor.hasNext();)
		{
			DBAuthor newAuthor = new DBAuthor();
			
			WSAuthor author = itAuthor.next();
			
			newAuthor.setAuthorID(author.getAuthorID());
			newAuthor.setName(author.getName());
			
			authorList.add(newAuthor);
		}

		this.setAuthor(authorList);
		
		PublisherList publishers = book.getPublisher();
		Collection<DBPublisher> publisherList = new ArrayList<DBPublisher>();
		
		for(Iterator<WSPublisher> itPublisher = publishers.iterator(); itPublisher.hasNext();)
		{
			DBPublisher newPublisher = new DBPublisher();
			
			WSPublisher publisher = itPublisher.next();
			
			newPublisher.setPublisherID(publisher.getPublisherID());
			newPublisher.setName(publisher.getName());
			newPublisher.setHomepage(publisher.getHomepage());
			
			publisherList.add(newPublisher);
		}
		
		this.setPublisher(publisherList);
	}

	/**
	 * @param libID the libID to set
	 */
	public void setBookID(long BookID) {
		this.bookID = BookID;
	}

	/**
	 * @return the libID
	 */
	@Id
	@Column()
	@GeneratedValue()
	public long getBookID() {
		return bookID;
	}

	/**
	 * @param title the title to set
	 */
	public void setTitle(String title) {
		this.title = title;
	}

	/**
	 * @return the title
	 */
	@Column()
	public String getTitle() {
		return title;
	}

	/**
	 * @param location the location to set
	 */
	public void setLocation(String location) {
		this.location = location;
	}

	/**
	 * @return the location
	 */
	@Column()
	public String getLocation() {
		return location;
	}

	/**
	 * @param isbn the isbn to set
	 */
	public void setIsbn(String isbn) {
		this.isbn = isbn;
	}

	/**
	 * @return the isbn
	 */
	@Column()
	public String getIsbn() {
		return isbn;
	}

	/**
	 * @param owner the owner to set
	 */
	public void setOwner(String owner) {
		this.owner = owner;
	}

	/**
	 * @return the owner
	 */
	@Column()
	public String getOwner() {
		return owner;
	}

	/**
	 * @param launch the launch to set
	 */
	public void setLaunch(Date launch) {
		this.launch = launch;
	}

	/**
	 * @return the launch
	 */
	@Column()
	public Date getLaunch() {
		return launch;
	}

	/**
	 * @param subject the subject to set
	 */
	public void setSubject(String subject) {
		this.subject = subject;
	}

	/**
	 * @return the subject
	 */
	@Column()
	public String getSubject() {
		return subject;
	}

	/**
	 * @param image the image to set
	 */
	public void setImage(String image)
	{
		this.image = image;
	}

	/**
	 * @return the image
	 */
	@Column()
	public String getImage()
	{
		return image;
	}

	/**
	 * @param description the description to set
	 */
	public void setDescription(String description) {
		this.description = description;
	}

	/**
	 * @return the description
	 */
	@Column()
	public String getDescription() {
		return description;
	}

	/**
	 * @param author the author to set
	 */
	public void setAuthor(Collection<DBAuthor> author)
	{
		this.author = author;
	}

	/**
	 * @return the author
	 */
	@ManyToMany()
	@JoinTable(name="BOOK_AUTHOR",
			joinColumns={@JoinColumn(name="BOOKID")},
			inverseJoinColumns={@JoinColumn(name="AUTHORID")})
	public Collection<DBAuthor> getAuthor()
	{
		return author;
	}

	/**
	 * @param publisher the publisher to set
	 */
	public void setPublisher(Collection<DBPublisher> publisher)
	{
		this.publisher = publisher;
	}

	/**
	 * @return the publisher
	 */
	@ManyToMany()
	@JoinTable(name="BOOK_PUBLISHER",
			joinColumns={@JoinColumn(name="BOOKID")},
			inverseJoinColumns={@JoinColumn(name="PUBLISHERID")})
	public Collection<DBPublisher> getPublisher()
	{
		return publisher;
	}
}