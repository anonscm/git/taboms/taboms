/*
 * tarent Book Management System - Server,
 * tarent Book Management System - Server
 * Copyright (C) 2000-2009 tarent GmbH
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License,version 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 *
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'tarent Book Management System - Server'
 * Signature of Elmar Geese, 21 April 2009
 * Elmar Geese, CEO tarent GmbH.
 */

/**
 * 
 */
package de.tarent.taBoMSServer.database.worker;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.Query;

import de.tarent.taBoMSServer.database.data.DBAuthor;
import de.tarent.taBoMSServer.database.data.DBBook;
import de.tarent.taBoMSServer.database.data.DBPublisher;

/**
 * @author Frederic Eßer
 *
 */
public class DBBookWorker
{
	/**
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public List<DBBook> getBooks()
	{
		EntityManagerFactory factory = Persistence.createEntityManagerFactory("books");
		EntityManager entityManager = factory.createEntityManager();
		Query query = entityManager.createQuery("SELECT l FROM DBBook l");
		
		List<DBBook> result = query.getResultList();

		entityManager.close();
		factory.close();
		
		return result;
	}
	
	/**
	 * @param library
	 */
	public void saveBook(DBBook books)
	{
		EntityManagerFactory factory = Persistence.createEntityManagerFactory("books");
		EntityManager entityManager = factory.createEntityManager();
		
		Collection<DBAuthor> dbAuthors = books.getAuthor();
		
		DBAuthorWorker dbAuthorWorker = new DBAuthorWorker();
		
		for(Iterator<DBAuthor> itAuthor = dbAuthors.iterator(); itAuthor.hasNext();)
		{
			DBAuthor newAuthor = itAuthor.next();
			
			if(newAuthor.getAuthorID() == 0)
			{
				dbAuthorWorker.saveAuthor(newAuthor);
			}
		}
		
		Collection<DBPublisher> dbPublisher = books.getPublisher();
		
		DBPublisherWorker dbPublisherWorker = new DBPublisherWorker();
		
		for(Iterator<DBPublisher> itPublisher = dbPublisher.iterator(); itPublisher.hasNext();)
		{
			DBPublisher newPublisher = itPublisher.next();
			
			if(newPublisher.getPublisherID() == 0)
			{
				dbPublisherWorker.savePublisher(newPublisher);
			}
		}
		
		entityManager.getTransaction().begin();
		entityManager.persist(books);
		entityManager.flush();
		entityManager.getTransaction().commit();
		
		entityManager.close();
		factory.close();
	}

	/**
	 * function to update a book
	 * 
	 * @param book
	 */
	public void updateBooks(DBBook book)
	{
		EntityManagerFactory factory = Persistence.createEntityManagerFactory("books");
		EntityManager entityManager = factory.createEntityManager();
		
		Collection<DBAuthor> dbAuthors = book.getAuthor();
		
		DBAuthorWorker authorWorker = new DBAuthorWorker();
		
		for(Iterator<DBAuthor> itAuthor = dbAuthors.iterator(); itAuthor.hasNext();)
		{
			DBAuthor newAuthor = itAuthor.next();
			
			authorWorker.updateAuthor(newAuthor);
		}
		
		Collection<DBPublisher> dbPublisher = book.getPublisher();
		
		DBPublisherWorker publisherWorker = new DBPublisherWorker();
		
		for(Iterator<DBPublisher> itPublisher = dbPublisher.iterator(); itPublisher.hasNext();)
		{
			DBPublisher newPublisher = itPublisher.next();
			
			publisherWorker.updatePublisher(newPublisher);
		}
		
		entityManager.getTransaction().begin();
		entityManager.merge(book);
		entityManager.flush();
		entityManager.getTransaction().commit();

		entityManager.close();
		factory.close();
	}

	/**
	 * @param id
	 */
	public void deleteBook(long id)
	{
		EntityManagerFactory factory = Persistence.createEntityManagerFactory("books");
		EntityManager entityManager = factory.createEntityManager();
		
		DBBook bookToRemove = entityManager.find(DBBook.class, id);

		entityManager.getTransaction().begin();
		entityManager.remove(bookToRemove);
		entityManager.flush();
		entityManager.getTransaction().commit();

		entityManager.close();
		factory.close();
	}
	
	/**
	 * function to return the book information with the book cover
	 * 
	 * @param <code>cover - String</code>
	 * @return book
	 */
	public DBBook searchBook(String cover)
	{
		EntityManagerFactory factory = Persistence.createEntityManagerFactory("books");
		EntityManager entityManager = factory.createEntityManager();
		
		Query query = entityManager.createQuery("SELECT b FROM DBBook b WHERE b.image = :value");
		query.setParameter("value", cover);
		
		DBBook book = (DBBook) query.getSingleResult();
		
		// before connection get closed get the relations
		book.getAuthor().size();
		book.getPublisher().size();

		// now all relations are saved close connection
		entityManager.close();
		factory.close();
		
		return book;
	}
	
	/**
	 * return all Book-covers from database 
	 * 
	 * @return coverList - ArrayList - List of all book-covers from the database 
	 */
	public ArrayList<String> getAllBookCovers()
	{
		EntityManagerFactory factory = Persistence.createEntityManagerFactory("books");
		EntityManager entityManager = factory.createEntityManager();
		
		Query query = entityManager.createQuery("SELECT image FROM DBBook");
		
		ArrayList<String> coverList = (ArrayList<String>) query.getResultList();
		
		entityManager.close();
		factory.close();
		
		return coverList;
	}
}