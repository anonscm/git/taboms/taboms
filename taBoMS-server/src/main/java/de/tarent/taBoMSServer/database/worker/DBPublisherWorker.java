/*
 * tarent Book Management System - Server,
 * tarent Book Management System - Server
 * Copyright (C) 2000-2009 tarent GmbH
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License,version 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 *
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'tarent Book Management System - Server'
 * Signature of Elmar Geese, 21 April 2009
 * Elmar Geese, CEO tarent GmbH.
 */

/**
 * 
 */
package de.tarent.taBoMSServer.database.worker;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.Query;

import de.tarent.taBoMSServer.database.data.DBPublisher;

/**
 * @author Frederic Eßer
 *
 */
public class DBPublisherWorker
{
	/**
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public List<DBPublisher> getPublisher()
	{
		EntityManagerFactory factory = Persistence.createEntityManagerFactory("books");
		EntityManager entityManager = factory.createEntityManager();
		Query query = entityManager.createQuery("SELECT p FROM DBPublisher p");
		
		List<DBPublisher> result = query.getResultList();

		entityManager.close();
		factory.close();
		
		return result;
	}
	
	/**
	 * function to write the publisher to the database
	 * 
	 * @param publisher
	 */
	public void savePublisher(DBPublisher publisher)
	{
		EntityManagerFactory factory = Persistence.createEntityManagerFactory("books");
		EntityManager entityManager = factory.createEntityManager();
		
		entityManager.getTransaction().begin();
		entityManager.persist(publisher);
		entityManager.flush();
		entityManager.getTransaction().commit();
		
		entityManager.close();
		factory.close();
	}

	/**
	 * @param id
	 * @param name
	 * @param homepage
	 */
	public void updatePublisher(DBPublisher publisher)
	{
		EntityManagerFactory factory = Persistence.createEntityManagerFactory("books");
		EntityManager entityManager = factory.createEntityManager();
		
		entityManager.getTransaction().begin();
		entityManager.merge(publisher);
		entityManager.flush();
		entityManager.getTransaction().commit();

		entityManager.close();
		factory.close();
	}

	/**
	 * @param id
	 */
	public void deletePublisher(long id)
	{
		EntityManagerFactory factory = Persistence.createEntityManagerFactory("books");
		EntityManager entityManager = factory.createEntityManager();
		
		DBPublisher publisherToRemove = entityManager.find(DBPublisher.class, id);

		entityManager.getTransaction().begin();
		entityManager.remove(publisherToRemove);
		entityManager.flush();
		entityManager.getTransaction().commit();

		entityManager.close();
		factory.close();
	}
}
