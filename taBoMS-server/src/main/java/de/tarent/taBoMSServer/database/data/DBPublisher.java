/*
 * tarent Book Management System - Server,
 * tarent Book Management System - Server
 * Copyright (C) 2000-2009 tarent GmbH
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License,version 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 *
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'tarent Book Management System - Server'
 * Signature of Elmar Geese, 21 April 2009
 * Elmar Geese, CEO tarent GmbH.
 */

package de.tarent.taBoMSServer.database.data;

import java.util.ArrayList;
import java.util.Collection;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToMany;

/**
 * @author Frederic Eßer
 *
 */
@Entity
public class DBPublisher
{
	private long publisherID;
	private String name;
	private String homepage;
	
	private Collection<DBBook> books = new ArrayList<DBBook>();

	/**
	 * @param publisherID the publisherID to set
	 */
	public void setPublisherID(long publisherID) {
		this.publisherID = publisherID;
	}

	/**
	 * @return the publisherID
	 */
	@Id
	@Column()
	@GeneratedValue()
	public long getPublisherID() {
		return publisherID;
	}

	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @return the name
	 */
	@Column()
	public String getName() {
		return name;
	}

	/**
	 * @param homepage the homepage to set
	 */
	public void setHomepage(String homepage) {
		this.homepage = homepage;
	}

	/**
	 * @return the homepage
	 */
	@Column()
	public String getHomepage() {
		return homepage;
	}

	/**
	 * @param library the library to set
	 */
	public void setLibrary(Collection<DBBook> library)
	{
		this.books = library;
	}

	/**
	 * @return the library
	 */
	@ManyToMany(mappedBy="publisher")
	public Collection<DBBook> getLibrary()
	{
		return books;
	}
}