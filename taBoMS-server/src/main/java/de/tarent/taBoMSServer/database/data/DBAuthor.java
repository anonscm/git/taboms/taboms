/*
 * tarent Book Management System - Server,
 * tarent Book Management System - Server
 * Copyright (C) 2000-2009 tarent GmbH
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License,version 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 *
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'tarent Book Management System - Server'
 * Signature of Elmar Geese, 21 April 2009
 * Elmar Geese, CEO tarent GmbH.
 */

package de.tarent.taBoMSServer.database.data;

import java.util.ArrayList;
import java.util.Collection;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToMany;

/**
 * @author Frederic Eßer
 *
 */
@Entity
public class DBAuthor
{
	private long authorID;
	private String name;
	
	private Collection<DBBook> library = new ArrayList<DBBook>();
	
	/**
	 * @param authorID the authorID to set
	 */
	public void setAuthorID(long authorID) {
		this.authorID = authorID;
	}
	
	/**
	 * @return authorID - long
	 */
	@Id
	@Column()
	@GeneratedValue()
	public long getAuthorID() {
		return authorID;
	}
	
	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}
	
	/**
	 * @return name - String
	 */
	@Column()
	public String getName() {
		return name;
	}

	/**
	 * @param library the library to set
	 */
	public void setLibrary(Collection<DBBook> library)
	{
		this.library = library;
	}

	/**
	 * @return library - Collection<DBBook>
	 */
	@ManyToMany(mappedBy="author")
	public Collection<DBBook> getLibrary()
	{
		return library;
	}
}