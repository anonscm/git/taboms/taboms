/*
 * tarent Book Management System - Server,
 * tarent Book Management System - Server
 * Copyright (C) 2000-2009 tarent GmbH
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License,version 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 *
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'tarent Book Management System - Server'
 * Signature of Elmar Geese, 21 April 2009
 * Elmar Geese, CEO tarent GmbH.
 */

/**
 * 
 */
package de.tarent.taBoMSServer.worker;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import javax.jws.WebMethod;

import org.apache.commons.validator.routines.ISBNValidator;

import com.amazonaws.a2s.AmazonA2S;
import com.amazonaws.a2s.AmazonA2SException;
import com.amazonaws.a2s.AmazonA2SLocale;
import com.amazonaws.a2s.model.*;
import com.amazonaws.a2s.*;

import de.tarent.octopus.content.annotation.Name;
import de.tarent.octopus.content.annotation.Optional;
import de.tarent.octopus.content.annotation.Result;
import de.tarent.octopus.server.OctopusContext;
import de.tarent.taBoMSServer.convert.WSAuthor;
import de.tarent.taBoMSServer.convert.WSBook;
import de.tarent.taBoMSServer.convert.WSPublisher;
import de.tarent.taBoMSServer.convert.lists.AuthorList;
import de.tarent.taBoMSServer.convert.lists.PublisherList;
import de.tarent.taBoMSServer.exception.TaBoMSException;

/**
 * class to handle the information request to amazon
 * 
 * @author Frederic Eßer
 *
 */
public class AmazonWorker
{
	public String accessKeyId = "0PHG7534QEQJFY2N2C82";			// generated at aws.amazon.com
	public String associateTag = "";							// optional
	public AmazonA2SLocale localDE = AmazonA2SLocale.DE;		// Local for DE server
	public AmazonA2SLocale localUS = AmazonA2SLocale.US;		// Local for US server
	public static String path;
	
	/**
	 * function to get the information of a book from the amazon web-service
	 * 
	 * @param oc
	 * @param isbn
	 * @throws TaBoMSException
	 */
	@WebMethod()
	@Result("WSBOOK")
	public WSBook getAmazonInfo(OctopusContext oc, @Name("code") @Optional(false) String code) 
	throws TaBoMSException
	{
		System.out.println("[INFO] AmazonWorker->getAmazonInfo: task called");
		ISBNValidator isbnValid = new ISBNValidator();
		
		// save the Octopus path
		path = oc.moduleConfig().getRealPath().toString();
		
		System.out.println(path);
		
		// check for valid ISBN / EAN
		if(isbnValid.isValid(code))
		{
			System.out.println("[INFO] AmazonWorker->getAmazonInfo: check ISBN: " + code);
			// ISBN -> length < 13
			// AEN 	-> length = 13
			if(code.length() != 13)
			{
				code = isbnValid.convertToISBN13(code);
			}
		} else {
			throw new TaBoMSException(108, "[Error:108] AmazonWorker->getAmazonInfo: code is not valid");
		}
		AmazonA2S serviceDE = requestInfo(this.localDE, this.accessKeyId, this.associateTag);
		
		ItemLookupRequest request = new ItemLookupRequest();
		
		request.getResponseGroup().add("EditorialReview");	// add the description
		request.getResponseGroup().add("Images");			// add Images
		request.getResponseGroup().add("ItemAttributes");	// add Attributes such as author or label
		request.setIdType("EAN");							// set id type to EAN (includes 10 and 13 digit ISBNs
		request.setSearchIndex("Books");
		request.getItemId().add(code);						// add the code (scanned code -> EAN)
		
		WSBook wsBook = null;
		
		try {
			wsBook = invokeItemLookup(serviceDE, request);
		} catch (AmazonA2SException e) {
			throw new TaBoMSException(109, "[Error:109] AmazonWorker->getAmazonInfo: Amazon Request error",e);
		} catch (IOException e) {
			throw new TaBoMSException(110, "[Error:110] AmazonWorker->getAmazonInfo:"
					+ "File not found / Read write permission failure",e);
		} catch (ParseException e) {
			throw new TaBoMSException(111, "[Error:111] AmazonWorker->getAmazonInfo: Date Parse error",e);
		}
		return wsBook;
	}

	/**
	 * function to create the service of AmazonA2S
	 * 
	 * @param local
	 * @param accessKeyId
	 * @param associateTag
	 * @return service
	 */
	private AmazonA2S requestInfo(AmazonA2SLocale local, String accessKeyId, String associateTag)
	{
		AmazonA2S service = new AmazonA2SClient(accessKeyId, accessKeyId, local);
		
		return service;
	}
	
	/**
	 * function to request information from amazon
	 * 
	 * @param service
	 * @param request
	 * @return wsBook
	 * @throws AmazonA2SException
	 * @throws IOException
	 * @throws ParseException 
	 */
	private static WSBook invokeItemLookup(AmazonA2S service, ItemLookupRequest request)
	throws AmazonA2SException, IOException, ParseException
	{
		WSBook wsBook = new WSBook();
		
		ItemLookupResponse response = service.itemLookup(request);
		
		List<Items> itemsList = response.getItems();
		
		for(Items items : itemsList)
		{
			List<Item> itemList = items.getItem();
			
			for(Item item : itemList)
			{
				/* *****************************
				 * Image
				 * *****************************/
				List<ImageSets> imageSetsList = item.getImageSets();
				for(ImageSets imageSets : imageSetsList)
				{
					List<ImageSet> imageSetList = imageSets.getImageSet();
					for(ImageSet imageSet : imageSetList)
					{
						if(imageSet.isSetLargeImage())	// check if large Image is set
						{
							Image largeImage = imageSet.getLargeImage();
							
							if(largeImage.isSetURL())
							{
								URL url =  new URL(largeImage.getURL());
								
								InputStream iStream = url.openConnection().getInputStream();
								
								// parse the FileName
								String u = largeImage.getURL();
								String[] uA = u.split("/");
								String fileName = uA[uA.length-1].toString();
								
								File file = new File(path+ "/../images/" + fileName);
								
								FileOutputStream fos = new FileOutputStream(file);
									
								int is;
								while((is = iStream.read()) != -1)
								{
									fos.write(is);
								}
								
								fos.flush();
								
								// close connections
								fos.close();
								iStream.close();
								
								// save cover in wsBook
								wsBook.setImage(fileName);
							}
						}
					}
				}
				/* *****************************
				 * Description
				 * *****************************/
				if(item.isSetEditorialReviews())
				{
					EditorialReviews editorialReviews = item.getEditorialReviews();
					
					List<EditorialReview> editorialReviewsList = editorialReviews.getEditorialReview();
					
					for(EditorialReview editorialReview : editorialReviewsList)
					{
						if(editorialReview.isSetContent())
						{
							wsBook.setDescription(editorialReview.getContent()); // save description in wsBook
						} else {
							wsBook.setDescription("");
						}
					}
				}
				
				/* *****************************
				 * Attributes
				 * *****************************/
				if(item.isSetItemAttributes())
				{
					ItemAttributes itemAttributes = item.getItemAttributes();
					
					/* *****************************
					 * Authors
					 * *****************************/
					List<String> authorList = itemAttributes.getAuthor();
					
					AuthorList listOfAuthors = new AuthorList();
					
					if(!itemAttributes.getAuthor().isEmpty())
					{
						for(String author : authorList)
						{
							WSAuthor newAuthor = new WSAuthor();
							newAuthor.setName(author);
						
							listOfAuthors.add(newAuthor);
						}
					} else {
						WSAuthor newAuthor = new WSAuthor();
						newAuthor.setName("");
					
						listOfAuthors.add(newAuthor);
					}
					
					// save authors in wsBook
					wsBook.setAuthor(listOfAuthors);
					
					/* *****************************
					 * Publisher
					 * *****************************/
					PublisherList listOfPublisher = new PublisherList();
					
					if(itemAttributes.isSetPublisher())
					{
						WSPublisher newPublisher = new WSPublisher();
						newPublisher.setName(itemAttributes.getPublisher());
						
						listOfPublisher.add(newPublisher);
					} else {
						WSPublisher newPublisher = new WSPublisher();
						newPublisher.setName("");
						
						listOfPublisher.add(newPublisher);
					}
					
					// save publisher to wsBook
					wsBook.setPublisher(listOfPublisher);
					
					/* *****************************
					 * Publication
					 * *****************************/
					if(itemAttributes.isSetPublicationDate())
					{
						int length = itemAttributes.getPublicationDate().length();
						Date newDate = null;
						SimpleDateFormat dateFormat = null;
						
						switch (length) {
						case 10:	// yyyy-MM-dd
							dateFormat = new SimpleDateFormat("yyyy-MM-dd");
							newDate = dateFormat.parse(itemAttributes.getPublicationDate());
							break;
						case 7:		// yyyy-MM
							dateFormat = new SimpleDateFormat("yyyy-MM");
							newDate = dateFormat.parse(itemAttributes.getPublicationDate());
							break;
						default:
							newDate = null;
							break;
						}
						
						wsBook.setLaunch(newDate);
					} else {
						Date newDate = null;
						wsBook.setLaunch(newDate);
					}
					
					/* *****************************
					 * Title
					 * *****************************/
					if(itemAttributes.isSetTitle())
					{
						wsBook.setTitle(itemAttributes.getTitle());
					} else {
						wsBook.setTitle("");
					}
					
					/* *****************************
					 * ISBN
					 * *****************************/
					if(itemAttributes.isSetISBN())
					{
						wsBook.setIsbn(itemAttributes.getISBN()); // set the ISBN for this book (just in case)
					}
				}
			}
		}
		return wsBook;
	}
}