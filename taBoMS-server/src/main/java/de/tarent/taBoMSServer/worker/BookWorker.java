/*
 * tarent Book Management System - Server,
 * tarent Book Management System - Server
 * Copyright (C) 2000-2009 tarent GmbH
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License,version 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 *
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'tarent Book Management System - Server'
 * Signature of Elmar Geese, 21 April 2009
 * Elmar Geese, CEO tarent GmbH.
 */

package de.tarent.taBoMSServer.worker;

import java.util.ArrayList;

import javax.jws.WebMethod;
import javax.persistence.EntityExistsException;
import javax.persistence.NoResultException;
import javax.persistence.NonUniqueResultException;
import javax.persistence.PersistenceException;
import javax.persistence.TransactionRequiredException;

import de.tarent.octopus.content.annotation.Name;
import de.tarent.octopus.content.annotation.Optional;
import de.tarent.octopus.content.annotation.Result;
import de.tarent.octopus.server.OctopusContext;
import de.tarent.taBoMSServer.convert.WSBook;
import de.tarent.taBoMSServer.database.data.DBBook;
import de.tarent.taBoMSServer.database.worker.DBAuthorWorker;
import de.tarent.taBoMSServer.database.worker.DBBookWorker;
import de.tarent.taBoMSServer.database.worker.DBPublisherWorker;
import de.tarent.taBoMSServer.exception.TaBoMSException;

/**
 * @author Frederic Eßer
 *
 */
public class BookWorker
{
	public static DBAuthorWorker dbAuthorWorker = new DBAuthorWorker();
	public static DBPublisherWorker dbPublisherWorker = new DBPublisherWorker();
	public static DBBookWorker dbBookWorker = new DBBookWorker();
	
	/**
	 * function to save a book to the database
	 * 
	 * @param oc
	 * @param book
	 * @throws TaBoMSException 
	 */
	@WebMethod()
	@Result()
	public void addBook(OctopusContext oc, 
			@Name("book") @Optional(false) WSBook book) throws TaBoMSException
	{
		System.out.println("[INFO] BookWorker->addBook: task called");
		try {
			dbBookWorker.saveBook(new DBBook(book));
		} catch (EntityExistsException e) {
			throw new TaBoMSException(100, "[Error:100] BookWorker->addBook: entity already exists", e);
		} catch (IllegalStateException e) {
			throw new TaBoMSException(101, "[Error:101] BookWorker->addBook: connection already closed", e);
		} catch (IllegalArgumentException e) {
			throw new TaBoMSException(102, "[Error:102] BookWorker->addBook: object is not an entity", e);
		} catch (TransactionRequiredException e) {
			throw new TaBoMSException(103, "[Error:103] BookWorker->addBook: there is no transaction", e);
		} catch (PersistenceException e) {
			throw new TaBoMSException(104, "[Error:104] BookWorker->addBook: flush failed", e);
		}
	}
	
	/**
	 * function to get all book-covers from the database
	 * 
	 * @param oc
	 * @return all book-covers
	 * @throws TaBoMSException 
	 */
	@WebMethod()
	@Result("COVERS")
	public ArrayList<String> getAllBooks(OctopusContext oc) throws TaBoMSException
	{
		System.out.println("[INFO] BookWorker->getAllBooks: task called");
		try {
			return dbBookWorker.getAllBookCovers();
		} catch (IllegalStateException e) {
			throw new TaBoMSException(101, "[Error:101] BookWorker->addBook: connection already closed", e);
		} catch (IllegalArgumentException e) {
			throw new TaBoMSException(102, "[Error:102] BookWorker->addBook: object is not an entity", e);
		}
	}

	/**
	 * function to get the information for a book by searching for the cover 
	 * 
	 * @param oc
	 * @param bookCover
	 * @return Web-Service book
	 * @throws TaBoMSException 
	 */
	@WebMethod()
	@Result("WSBOOK")
	public WSBook getBookInfo(OctopusContext oc, 
			@Name("cover") @Optional(false) String bookCover) throws TaBoMSException
	{
		System.out.println("[INFO] BookWorker->getBookInfo: task called");
		try {
			DBBook book = dbBookWorker.searchBook(bookCover);
			return new WSBook(book);
		} catch (IllegalStateException e) {
			throw new TaBoMSException(101, "[Error:101] BookWorker->getBookInfo: connection already closed", e);
		} catch (IllegalArgumentException e) {
			throw new TaBoMSException(102, "[Error:102] BookWorker->getBookInfo: parameter does not correspond"
					+ "to parameter in query string or type of argument is incorrect", e);
		} catch (NoResultException e) {
			throw new TaBoMSException(105, "[Error:105] BookWorker->getBookInfo: no results", e);
		} catch (NonUniqueResultException e) {
			throw new TaBoMSException(106, "[Error:106] BookWorker->getBookInfo: more than one result", e);
		}
	}
	
	/**
	 * function to change update the informations of the Book, Author and Publisher
	 * 
	 * @param oc
	 * @param book
	 * @throws TaBoMSException 
	 */
	@WebMethod()
	@Result()
	public void changeBookInfo(OctopusContext oc, 
			@Name("book") @Optional(false) WSBook book) throws TaBoMSException
	{
		System.out.println("[INFO] BookWorker->changeBookInfo: task called");
		try {
			dbBookWorker.updateBooks(new DBBook(book));			
		} catch (IllegalStateException e) {
			throw new TaBoMSException(101, "[Error:101] BookWorker->changeBookInfo: connection already closed", e);
		} catch (IllegalArgumentException e) {
			throw new TaBoMSException(107, "[Error:107] BookWorker->changeBookInfo: instance is not an entity"
					+ "or is a removed entity", e);
		} catch (TransactionRequiredException e) {
			throw new TaBoMSException(103, "[Error:103] BookWorker->changeBookInfo: there is no transaction", e);
		} catch (PersistenceException e) {
			throw new TaBoMSException(104, "[Error:104] BookWorker->changeBookInfo: flush failed", e);
		}
	}
	
	/**
	 * function to remove a book from the database
	 * 
	 * @param oc
	 * @param book
	 * @throws TaBoMSException 
	 */
	@WebMethod()
	@Result()
	public void deleteBook(OctopusContext oc, @Name("id") @Optional(false) WSBook book) throws TaBoMSException
	{
		System.out.println("[INFO] BookWorker->deleteBook: task called");
		try {
			dbBookWorker.deleteBook(book.getBookID());
		} catch (IllegalStateException e) {
			throw new TaBoMSException(101, "[Error:101] BookWorker->deleteBook: connection already closed", e);
		} catch (IllegalArgumentException e) {
			throw new TaBoMSException(107, "[Error:107] BookWorker->deleteBook: instance is not an entity"
					+ "or is a removed entity", e);
		} catch (TransactionRequiredException e) {
			throw new TaBoMSException(103, "[Error:103] BookWorker->deleteBook: there is no transaction", e);
		} catch (PersistenceException e) {
			throw new TaBoMSException(104, "[Error:104] BookWorker->deleteBook: flush failed", e);
		}
	}
}